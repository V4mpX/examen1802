<!doctype html>
<html lang="en">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>
  <?php require "../app/views/parts/header.php" ?>
  <main role="main" class="container">
    <div class="starter-template">
      <h1>Crear Jugador</h1>
      <form method="post" action="/jugador/store">
        <div class="form-group">
          <label>Nombre</label>
          <input type="text" name="nombre" class="form-control" required="required">
        </div>
        <div class="form-group">
          <label>Nacimiento</label>
          <input type="date" name="nacimiento" class="form-control" required="required">
        </div>
        <div class="form-group">
          <label>Puesto</label>
          <select name="id_puesto" class="form-control">
            <?php foreach ($jugadortypes as $jugadortype): ?>
            <option value="<?php echo $jugadortype->id ?>" > <?php echo $jugadortype->nombre ?> </option>
            <?php endforeach ?>
          </select>
        </div>
        <button type="submit" class="btn btn-default">Crear</button>
      </form>
    </div>
    <a href="/jugador">Volver</a>
  </main>
  <?php require "../app/views/parts/footer.php" ?>
</body>
  <?php require "../app/views/parts/scripts.php" ?>
