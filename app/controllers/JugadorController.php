<?php
namespace App\Controllers;

//require_once '../app/models/User.php';

use \App\Models\Jugador;

/**
*
**/

Class JugadorController
{

    function __construct(){
    }

    public function index(){

        $pagesize = 7;
        $users = Jugador::paginate($pagesize);
        $rowCount = Jugador::rowCount();


        $pages = ceil($rowCount / $pagesize);

        if (isset($_REQUEST['page'])) {
            $page = (integer)$_REQUEST['page'];
        }else{
            $page = 1;
        }

        require "../app/views/jugador/index.php";
    }

    public function titular(){
        require "../app/views/jugador/titular.php";
    }

    public function create(){
        $jugadortypes = Jugador::all_puesto();
        require "../app/views/jugador/create.php";
    }

    /*public function unset(){
        unset($_SESSION['titulares']);
        require "../app/views/user/titular.php";
    }*/

    public function store(){
        $jugador = new jugador();

        $jugador->nombre = $_REQUEST['nombre'];
        $jugador->nacimiento = $_REQUEST['nacimiento'];
        $jugador->id_puesto = $_REQUEST['id_puesto'];
        $jugador->insert();

        header('Location:/jugador');
    }

    public function add($arguments){
        $id = (int) $arguments[0];
        $jugador = Jugador::find($id);


        $_SESSION['titulares'][$jugador->id] = $jugador;

        header('Location: /jugador/titular');
    }

    public function remove($arguments){
        $id = (int) $arguments[0];
        $user = Jugador::find($id);

        unset($_SESSION['titulares'][$user->id]);

        header('Location: /jugador/titular');
    }

}
