<!doctype html>
<html lang="en">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>
  <?php require "../app/views/parts/header.php" ?>
  <main role="main" class="container">
    <div class="starter-template">
      <h1>Tabla usuarios</h1>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Nacimiento</th>
          <th>Puesto</th>
          <th>Titular</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($users as $user): ?>
        <tr>
          <td><?php echo $user->id ?></td>
          <td><?php echo $user->nombre ?></td>
          <td><?php echo $user->nacimiento->format("d-m-Y") ?></td>
          <td><?php echo $user->puesto->nombre ?></td>
          <td><a href="/jugador/add/<?php echo $user->id ?>"><button class='btn btn-primary'>Añadir</button></a>
        </tr>
      <?php endforeach ?>
      </tbody>
    </table>
    <?php for ($i=1; $i <= $pages ; $i++) {?>
      <?php if ($i != $page): ?>
        <a href="/jugador?page=<?php echo $i ?>" class="btn"> <?php echo $i ?></a>
      <?php else: ?>
        <span class="btn"><?php echo $i ?></span>
      <?php endif ?>
    <?php } ?>
  <hr>
  <a href="/jugador/create"><button class='btn btn-primary'>Crear</button></a>
  <!--<a href="/user/unset"><button class='btn btn-primary'>Unset</button></a>-->
  </main>
  <?php require "../app/views/parts/footer.php" ?>
</body>
  <?php require "../app/views/parts/scripts.php" ?>
