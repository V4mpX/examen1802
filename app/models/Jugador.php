<?php
namespace App\Models;

use PDO;
use Core\Model;

/**
*
*/
class Jugador extends Model
{

    function __construct(){
        $this->nacimiento = new \DateTime($this->nacimiento);
    }

    public static function all(){
        $db = Jugador::db();
        $statement = $db->query('SELECT * FROM jugadores');
        $users = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);

        return $users;
    }

    public static function all_puesto(){
        $db = Jugador::db();
        $statement = $db->query('SELECT * FROM puestos');
        $users = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);

        return $users;
    }

    public static function find($id){
        $db = Jugador::db();
        $stmt = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS,Jugador::class);
        $user = $stmt->fetch(PDO::FETCH_CLASS);

        return $user;
    }

    public function paginate($size = 10){
        if (isset($_REQUEST['page'])) {
            $page = (integer)$_REQUEST['page'];
        }else{
            $page = 1;
        }
        $offset = ($page - 1) * $size;

        $db = Jugador::db();
        $statememt = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');

        $statememt->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statememt->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statememt->execute();

        $users = $statememt->fetchAll(PDO::FETCH_CLASS,Jugador::class);

        return $users;
    }

    public static function rowCount(){
     $db = Jugador::db();
        $statememt = $db->prepare('SELECT count(id) as count FROM jugadores');

        $statememt->execute();

        $rowCount = $statememt->fetch(PDO::FETCH_ASSOC);

        return $rowCount['count'];
    }

    public function insert(){
        $db = Jugador::db();
        $stmt = $db->prepare('INSERT INTO jugadores(nombre, nacimiento, id_puesto) VALUES(:nombre, :nacimiento, :id_puesto)');

        $stmt->bindValue(':nombre', $this->nombre);
        $stmt->bindValue(':nacimiento', $this->nacimiento);
        $stmt->bindValue(':id_puesto', $this->id_puesto);

        return $stmt->execute();
    }

    public function puesto(){
        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM puestos WHERE id = :id');
        $statement->bindValue(':id', $this->id_puesto);
        $statement->execute();
        $puestos = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class)[0];

        return $puestos;
    }

    public function __get($atributoDesconocido){
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }
}
