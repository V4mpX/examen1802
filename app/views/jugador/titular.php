<!doctype html>
<html lang="en">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>
  <?php require "../app/views/parts/header.php" ?>
  <main role="main" class="container">
    <div class="starter-template">
      <h1>Titulares</h1>
    </div>
    <?php if (isset($_SESSION['titulares'])): ?>
      <?php if ($_SESSION['titulares']!=null): ?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Nacimiento</th>
              <th>ID_Puesto</th>
              <th>Titular</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($_SESSION['titulares'] as $user): ?>
              <tr>
                <td><?php echo $user->nombre ?></td>
                <td><?php echo $user->nacimiento->format("d-m-Y") ?></td>
                <td><?php echo $user->puesto->nombre ?></td>
                <td><a href="/jugador/remove/<?php echo $user->id ?>">X</a></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      <?php else: ?>
        <a href="/jugador"><button class='btn btn-primary'>Añade algun titular</button></a>
      <?php endif ?>
    <?php else: ?>
      <a href="/jugador"><button class='btn btn-primary'>Añade algun titular</button></a>
    <?php endif ?>
    <hr>
  </main>
  <?php require "../app/views/parts/footer.php" ?>
</body>
  <?php require "../app/views/parts/scripts.php" ?>
